# Projet DOM
Où on va voir comment manipuler le DOM avec javascript. (c'est un projet snowpack)

## Exercices DOM [js](src/exo-dom.js)/[html](public/exo-dom.html)
1. Mettre le texte de l'élément avec l'id para2 en bleu
2. Mettre une border en pointillet noire à la section2
3. Mettre une background color orange à l'élément de la classe colorful de la section2 
4. Mettre le h2 de la section1 en italique
5. Cacher l'élément colorful situé dans un paragraphe
6. Changer le texte de para2 pour "modified by JS"
7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
8. Rajouter la classe big-text sur le h2 de la section2
9. Bonus : Faire que tous les paragraphe du document soit en italique


## Exercice Compteur [js](src/exo-counter.js)/[html](public/exo-counter.html)
Dans le html, mettre un paragraphe avec un id counter et 2 button, un avec un id increment et l'autre avec un id decrement
1. Dans le JS, capturer les 3 éléments avec des querySelector et les mettre dans des variables
2. Créer une quatrième variable qui va contenir la valeur actuelle du compteur initialisée à zéro
3. Rajouter un EventListener sur le button increment pour faire que quand on click dessus, ça incrémente la valeur du compteur et 1 et ça l'affiche en console
4. Faire l'inverse pour le button decrement
5. Enfin, faire en sorte que lorsqu'on click sur l'un ou l'autre des button, ça vienne mettre à jour le texte du paragraphe pour lui faire afficher la valeur du compteur
6. Faire que si on double click sur le paragraphe, ça remette la compteur à zéro
7. Bonus: Faire que l'incrémentation/decrémentation + mise à jour de l'affichage se fasse via une seule fonction qui sera appelée dans les 2 eventlistener des buttons

## Exercice des carrés qui bougent [js](src/exo-event.js)/[html](public/exo-event.html)
1. Créer dans le HTML une div avec une classe square et faire une règle css pour qu'il ressemble à un carré (un carré rouge par exemple), en position absolute (Rajouter également une div #playground qui fera 100 vh et 100 vw)
2. ns le javascript, capturer la div#playground et lui rajouter un événement au click qui fera un console log de la position de la souris. Pour ça, vous aurez besoin du paramètre event sur la fonction du eventlistener (je vous laisse chercher)
3. Capturer également le carré rouge, et faire en sorte que lorsqu'on clique sur playground, celui ci se "téléporte" à l'endroit où on a cliqué. aide :  il va falloir changer son top et son left en leur donnant les positions x et y de la souris 
4. (si on est d'humeur fancy, pourquoi pas rajouter une petite transition en css pour faire qu'au lieu de se téléporter, il glisse avec grâce jusqu'à sa destination)
5. Ajouter un second carré dans le HTML (attention, lui il ne doit pas avoir de transition en css) et capturer le dans le JS
6. Rajouter un événement sur le playground pour quand la souris bouge, et faire en sorte de changer la position du carré2 pour la position actuelle de la souris, exactement comme avant en fait, mais sur un event et un carré différent
7. Bonus: faire en sorte que le carré commence à suivre quand on appuie le bouton de la souris sur lui et qu'il arrête lorsqu'on relâche le bouton de la souris


## Exo Liste de chiens
### I. La liste et le formulaire
1. Créer une variable qui va contenir un tableau avec dedans des chiens qui ressemblent à ça : `{id: 1, name:'Rex', breed: 'Corgi', age: 1}`
2. Créer une fonction addDog qui attendra en argument un name en string, une breed en string et un age en number, et faire que dans cette fonction, on push dans le tableau un nouvel objet chien avec les valeurs passées en argument (il faudra aussi faire en sorte que l'id s'incrémente à chaque fois)
3. Dans le HTML, créer un formulaire avec 3 inputs (name, breed et age) et côté javascript, rajouter un eventListener au submit de ce form pour en récupérer les valeurs de ses inputs et les afficher en console déjà par exemple
4.  Une fois qu'on arrive à choper ces valeurs, eh bien ya pu qu'à appeler la fonction addDog et si on fait un console log du tableau de chien, on devrait avoir un nouveau chien dedans
### II. Affichage des données en HTML
1.  Créer un élément ul dans le HTML, puis dans le js, faire une boucle qui va parcourir tous les chiens, et pour chaque chien va créer un élément li avec les informations du chien en question affichées dedans. Aide: En gros on fait un for...of sur le tableau, et dans ce for..of on fait un createElement li et on assigne au textContent de ce li le name et la breed du chien concaténées par exemple, et pis on l'append dans la ul qu'on a mis dans le HTML et préalablement capturé avec un querySelector
2.  Comme cet affichage on va le rappeler régulièrement, ça peut être une bonne idée d'en faire une fonction genre displayDogs() (Bonus: faire qu'on puisse donner à la fonction le tableau de chien à afficher ainsi que l'élément cible dans lequel on veut append les nouveaux chiens)
3.  Faire en sorte d'appeler la fonction displayDogs() au moment où on vient de rajouter un chien dans le tableau afin de mettre l'affichage à jour (il est probable que ça répète la liste des chiens, il va donc falloir modifier la fonction pour remettre à zéro le contenu HTML de la ul avant de boucler sur les chiens)
4. Bonus : afficher les chiens sous forme d'accordéon Bootstrap par exemple, avec juste le nom affiché de base, et faut cliquer sur l'accordion pour afficher sa breed, son age, son id
### III. Supprimer des données
1.  Créer une fonction removeDog(id) qui va attendre un number en argument, et dans cette fonction deux solution possibles :
    * Soit utiliser la méthode .filter(...) sur le tableau de chien pour retirer le chien dont l'id correspond à celui donné en argument
    * Soit faire une boucle sur for classique sur le tableau, et vérifier à chaque tour de boucle si l'id du chien correspond à l'id donné, et si oui, on fait un splice en lui donnant l'index du for actuel
2. On modifie la fonction display dog pour faire en sorte de rajouter dans le li de la boucle un button "remove"
3. On rajoute un eventlistener sur ce bouton et on lui fait déclencher la fonction removeDog ainsi que la fonction displayDogs