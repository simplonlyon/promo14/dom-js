let promo = ['Kamel','Ling', 'Jean', 'Quentin','Lelia','Christopher','Jehane','Amira','Nasrine','Riad','Andy','Thanhmy','Henda','Hedwig','Kevin','Fanny','Daniela'];

let section = document.querySelector('#target');


/** version function */

for(let student of promo) {

    createArticle(student);
}

const form = document.querySelector('form');
const input = document.querySelector('#student-name');

form.addEventListener('submit', (event) => {
    event.preventDefault();
    //version sous forme d'objet
    // console.log(new FormData(form).get('student-name'));

    createArticle(input.value);
});

/**
 * Une fonction permettant de créer un nouvel élément html, de type article
 * et qui ensuite l'append dans l'élément section
 * @param {string} text Le texte qui sera mis dans le nouvel élément créé
 */
function createArticle(text) {
    
    let article = document.createElement('article');
    article.textContent = text;
    
    article.addEventListener('click', () => {
        alert('Hello my name is '+text+' from promo14');
    });
    
    section.appendChild(article);
}


/** Version sans fonction */
/*
for(let student of promo) {

    let article = document.createElement('article');
    article.textContent = student;
    
    article.addEventListener('click', () => {
        alert('Hello my name is '+student+' from promo14');
    });
    
    section.appendChild(article);
}

const button = document.querySelector('button');

button.addEventListener('click', () => {
    
    let article = document.createElement('article');
    article.textContent = 'student';
    
    article.addEventListener('click', () => {
        alert('Hello my name is '+'student'+' from promo14');
    });
    
    section.appendChild(article);
});

*/