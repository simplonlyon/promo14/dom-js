
let paragraphe = document.querySelector('p');

console.log(paragraphe.textContent);

paragraphe.textContent = 'Hello Promo14';

console.log(paragraphe.textContent);


let button = document.querySelector('button');

button.addEventListener('click', () => {
    console.log('button clicked');
});


const target = document.querySelector('#target');

//On peut utiliser la fonction createElement pour créer une nouvelle
//balise HTML via JS, on lui indique ici le type, et ça nous renvoie
//exactement la même chose que si on avait fait un querySelector, mais
//là c'est pour un élément qui n'existe pas encore dans la page
const createdPara = document.createElement('p');
//On peut donc le manipuler comme n'importe quel autre élément du DOM
createdPara.textContent = 'Created by JS';
createdPara.classList.add('big-text');
createdPara.id = 'mon-id';
//même pourquoi pas lui rajouter un eventlistener
createdPara.addEventListener('click', () => {
    console.log('coucou');
});
//Et pour qu'il soit affiché, on utilise la fonction appendChild sur
//l'élément dans lequel on veut le mettre. Ici, on met notre paragraphe
//dans la balise target capturée plus haut
target.appendChild(createdPara);