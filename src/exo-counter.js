/** @type {HTMLElement} */
const btnIncrement = document.querySelector('#increment');
/** @type {HTMLElement} */
const btnDecrement = document.querySelector('#decrement');
/** @type {HTMLElement} */
const spanCounter = document.querySelector('#counter');

let counter = 0;

btnIncrement.addEventListener('click', () => {
    counter++;
    spanCounter.textContent = counter;

    
    //modifyCounter(1)
});

btnDecrement.addEventListener('click', () => {    
    counter--;
    spanCounter.textContent = counter;
    
    
    //modifyCounter(-1)

});

spanCounter.addEventListener('dblclick', () => {
    counter = 0;
    spanCounter.textContent = counter;
    
    //modifyCounter(-counter)
});


function modifyCounter(id) {
    counter += id;
    spanCounter.textContent = counter;
}
/*
function modifyCounter(id) {
    if(id === 'increment') {
        counter++;
    } else if(id === 'decrement') {
        counter--;
    } else {
        counter = 0;
    }
    spanCounter.textContent = counter;

}*/