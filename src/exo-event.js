
/** @type {HTMLElement} */
let playground = document.querySelector('#playground');

/** @type {HTMLElement} */
let redSquare = document.querySelector('#click-move');

playground.addEventListener('click', (event) => {
    console.log(event);
    let x = event.clientX;
    let y = event.clientY;
    // console.log(`x: ${x} y: ${y}`);
    console.log('x: ' + x + 'y:' + y);
    redSquare.style.left = x + 'px';
    redSquare.style.top = y + 'px';

});

/** @type {HTMLElement} */
let greenSquare = document.querySelector('#follow');
let drag = false;

greenSquare.addEventListener('mousedown', () => {
    drag = true;
});
greenSquare.addEventListener('mouseup', () => {
    drag = false;
});

playground.addEventListener('mousemove', (event) => {
    // if(event.buttons === 1) { //marche aussi, mais le carré suivra quelque soit là où on click
    if(drag) {
        let x = event.clientX;
        let y = event.clientY;
    
        greenSquare.style.left = x + 'px';
        greenSquare.style.top = y + 'px';

    }
    
});