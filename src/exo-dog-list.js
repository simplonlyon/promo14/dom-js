let dogList = [
    { id: 1, name: 'Rex', breed: 'Corgi', age: 2 },
    { id: 2, name: 'Bloup', breed: 'Doge', age: 3 },
    { id: 3, name: 'Patouf', breed: 'Pinscher', age: 14 },
];
let dogId = 4;


/**
 * Function that adds a new dog in the list
 * @param {string} paramName name of the dog
 * @param {string} paramBreed breed of the dog
 * @param {number} paramAge age of the dog
 */
function addDog(paramName, paramBreed, paramAge) {

    dogList.push({
        id: dogId,
        name: paramName,
        breed: paramBreed,
        age: paramAge
    });
    dogId++;
    
}

/**
 * Fonction qui retire un chien de la liste en se basant sur son id
 * @param {number} id l'id du chien à supprimer
 */
function removeDog(id) {
    // for (let index = 0; index < dogList.length; index++) {
    //     const dog = dogList[index];
    //     if(dog.id === id) {
    //         dogList.splice(index, 1);
    //         break;
    //     }
    // }

    dogList = dogList.filter(dog => dog.id !== id);
}





let form = document.querySelector('form');
let inputAge = document.querySelector('#age');
let inputName = document.querySelector('#name');
let inputBreed = document.querySelector('#breed');

form.addEventListener('submit', (event) => {
    event.preventDefault();

    addDog(inputName.value,
        inputBreed.value,
        Number(inputAge.value));
        
    displayDogs();
    
    console.log(dogList);
});

displayDogs();

function displayDogs() {
    const list = document.querySelector('#list');

    list.innerHTML = '';

    for (const dog of dogList) {
        
        const li = document.createElement('li');
        li.textContent = dog.name;
        list.appendChild(li);

        const removeBtn = document.createElement('button');
        removeBtn.textContent = 'remove';
        removeBtn.classList.add('btn', 'btn-danger')
        removeBtn.addEventListener('click', () => {
            removeDog(dog.id);
            displayDogs();
        });
        li.appendChild(removeBtn);
        //ça c'est pour le bonus
        // list.appendChild(createAccordionItem(dog));
    }
}










/**
 * Fonction pour le bonus qui permet de créer un item d'accordéon
 * bootstrap, c'est long et chiant à faire
 * @param {object} dog le chien
 * @returns {HTMLElement} l'item d'accordéon a append où on le souhaite
 */
function createAccordionItem(dog) {
    let accordionItem = document.createElement('div');
    accordionItem.classList.add('accordion-item');

    let accordionHeader = document.createElement('h3');
    accordionHeader.classList.add('accordion-header');
    accordionItem.appendChild(accordionHeader);
    accordionItem.id = 'heading'+dog.id;
    
    let accordionButton = document.createElement('button');
    accordionButton.classList.add('accordion-button', 'collapsed');
    accordionButton.setAttribute('data-bs-toggle', 'collapse');
    accordionButton.setAttribute('data-bs-target', '#collapse'+dog.id);
    accordionButton.setAttribute('aria-expanded', 'true');
    accordionButton.setAttribute('aria-controls', 'collapse'+dog.id);
    accordionButton.textContent = dog.name;
    accordionHeader.appendChild(accordionButton);

    let accordionCollapse = document.createElement('div');
    accordionCollapse.classList.add('accordion-collapse', 'collapse');
    accordionCollapse.setAttribute('aria-labelledby', 'heading'+dog.id);
    accordionCollapse.setAttribute('data-bs-parent', '#list');
    accordionCollapse.id = 'collapse'+dog.id;
    accordionItem.appendChild(accordionCollapse);

    let accordionBody = document.createElement('div');
    accordionBody.classList.add('accordion-body');
    accordionBody.innerHTML = `
    Id: ${dog.id} <br>
    Name: ${dog.name} <br>
    Breed: ${dog.breed} <br>
    Age: ${dog.age}
    `
    accordionCollapse.appendChild(accordionBody);

    return accordionItem;






}