/** @type {HTMLElement} */
let para2 = document.querySelector('#para2');
para2.style.color = 'blue';

/** @type {HTMLElement} */
let section2 = document.querySelector('#section2');
section2.style.border = '2px dotted black';

/** @type {HTMLElement} */
let sect2Colorful = document.querySelector('#section2 .colorful');
sect2Colorful.style.backgroundColor = 'orange';

/** @type {HTMLElement} */
let sect1H2 = document.querySelector('#section1 h2');
sect1H2.style.fontStyle = 'italic';

/** @type {HTMLElement} */
let pColorful = document.querySelector('p .colorful')
pColorful.style.display = 'none';

para2.textContent = 'Modified By JS';


let a = document.querySelector('a');
a.href = 'https://www.simplonlyon.fr';

//le classList nous permet de manipuler plus facilement les classes css
//présentent sur l'élément sélectionné
sect2Colorful.classList.add('big-text');

//Le querySelectorAll nous renvoie un tableau d'éléments correspondants
//au sélecteur CSS donné
let paras = document.querySelectorAll('p');
//Il faut donc faire une boucle pour parcourir le tableau et effectuer
//la modification sur chaque élément de celui ci
for(let p of paras) {
    p.style.fontStyle = 'italic';

} 